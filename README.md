# Oefenopdracht Algemene Rekenkamer

Deze opdracht geeft een idee van hoe we met data-analyse binnen de Algemene Rekenkamer werken. De code geeft een gelegenheid om te oefenen met een aantal werkzaamheden die zullen lijken op (een deel van) het werk binnen de Data Hub van de AR.

# Doel van deze opdracht
Het doel van deze opdracht is om in het tweede gesprek verder te kunnen doorpraten over het werk binnen de Data Hub. Om dit gesprek te fasciliteren hebben we een aantal vragen bij de data en code in dit project.

# Opzet code
Binnen onze dataprojecten gebruiken we een vaste mappenstructuur die ook in deze opdracht terugkomt.

```
Project
| README.md
├── 0_data
├── 1_functies
├── 2_opwerking
├── 3_analyses
├── 4_output
└── 5_achtergrond
```

| Mapnaam     |                                                    Beschrijving |
| ----------- | --------------------------------------------------------------- |
| 0_data      | Map met daarin de ruwe data die de AR voor het onderzoek ontvangen heeft. Het is aan raden om deze in een aparte map binnen deze map te zetten. Ook bewerkte databestanden kunnen opgeslagen worden in een aparte submap binnen de map 0_data. |
| 1_functies   | Deze map kan gebruikt worden om ondersteunende functies op te slaan die je in markdowncode voor de opwerking of analyses oproept. |
| 2_opwerking   | Hierin staat het markdownbestand waarin de code en uitleg staat over hoe de ruwe data bewerkt wordt om tot analyseerbare data te komen. Het gaat dan om de stappen inlezen, opschonen, bewerken en opslaan. Dus alle stappen die gezet worden om van ruwe data tot analyseerbare data te komen.        |
| 3_analyses   | Hierin staat het markdownbestand waarin de code en uitleg staan over hoe de data geanalyseerd en/of gevisualiseerd worden. Dus alle stappen van het inlezen van de opgeschoonde data tot de output die in nota’s of rapportage terechtkomen. |
| 4_output   | Hierin staat alle output die in de nota’s en/of rapportage (kunnen) gebruikt worden. |
| 5_achtergrond   | Hierin kunnen documenten opgeslagen worden met achtergrondinformatie over het project. Bijvoorbeeld het projectvoorstel en relevante rapporten en/of literatuur. |

# Spelregels
1. Mocht je vragen hebben, twijfel dan niet om contact op te nemen!
2. Mocht een van de vragen teveel tijd kosten, twijfel niet om te stoppen. Het gaat ons er niet om dat de vragen opgelost worden, maar vooral om het te kunnen hebben over je aanpak/ervaringen. Dat kan ook zonder dat je de oplossing (al) gevonden hebt.
3. Deel de resultaten 24 uur voor je gesprek met ons zodat we het gesprek kunnen voorbereiden. 
4. De vorm waarin je de oprdracht deel mag je kiezen: maar denk bijvoorbeeld aan het delen van de gehele git-repo of een of meerdere code en html bestanden.
4. Delen van je resultaten (push) via gitlab is niet mogelijk ivm privacyoverwegingen. Daarom vragen we je om de resultaten via https://fileshare.rekenkamer.nl/ aan e.ufkes@rekenkamer.nl en t.le@rekenkamer.nl te sturen. Klik op Verzenden en volg de instructies (vergeet niet om het wachtwoord ook met ons te delen).

## Vraag 1
Lukt het om het dashboard te clonen, te draaien en om wijzigingen aan te brengen? Maak een clone van de code zoals deze op Gitlab staat, maak een nieuwe branche aan, draai en verken het dashboard. Eén tip: het dashboard wordt gestart in het bestand `global.R`.

## Vraag 2
Het dashboard is gemodeleerd naar de oude huisstijl van de AR. Dat houdt in dat de kleuren, lettertypes en logo's volgens vaste voorschriften zijn gedefinieerd.  De code waarin deze stijl wordt beschreven staat in `style.css` in de map `www`

Stel je voor er komt een nieuwe huisstijl (Zie bijvoorbeeld onze [website](https://www.rekenkamer.nl/)): er komt een ander logo, andere kleuren en een ander lettertype. Welke dit zijn mag je zelf uitkiezen: kies een aantal kleuren die je mooi vindt, je favoriete lettertype en een logo en pas deze toe op dit dashboard. Pas de code zo aan dat het dashboard in de nieuwe huisstijl te zien is.

## Vraag 3
Op de eerste pagina van het dashboard staan drie zogenoemde *valueboxes*. De code voor deze boxes staan in `server.R` (regel 92 - 137) en in `1_functies/custom_vbox.R`. Wanneer je in het dashboard op zo'n box klikt dan linkt de box naar tabblad twee. De link zit echter alleen onder de tekst van de box, niet onder de box zelf. Een wens voor de toekomst is om de hele box klikbaar te hebben: hoe zou je dit aanpakken?

## Vraag 4
Wat is de eerste verbetering/aanpassing die jij aan dit dashboard of de onderliggende code zou willen uitvoeren? Als je wilt/tijd hebt kun je hier natuurlijk een begin mee maken.

